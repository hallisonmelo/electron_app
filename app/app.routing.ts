import {
  NgModule,
  Provider,
  Component
} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {ModuleWithProviders} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { InsercaoComponent } from './insercao/insercao.component';
import { ListagemComponent } from './listagem/listagem.component';



const APP_ROUTES: Routes = [
{path: '', component: InsercaoComponent},
{path: 'listagem', component: ListagemComponent},

];

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);

    