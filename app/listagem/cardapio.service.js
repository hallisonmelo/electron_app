"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var CardapioService = (function () {
    function CardapioService() {
        this.showID = new core_1.EventEmitter();
    }
    CardapioService.prototype.getID = function () {
        return this.id;
    };
    CardapioService.prototype.setID = function (id) {
        this.id = id;
        this.showID.emit(id);
    };
    return CardapioService;
}());
exports.CardapioService = CardapioService;
//# sourceMappingURL=cardapio.service.js.map