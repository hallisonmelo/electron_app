"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var insercao_component_1 = require("./insercao/insercao.component");
var listagem_component_1 = require("./listagem/listagem.component");
var APP_ROUTES = [
    { path: '', component: insercao_component_1.InsercaoComponent },
    { path: 'listagem', component: listagem_component_1.ListagemComponent },
];
exports.routing = router_1.RouterModule.forRoot(APP_ROUTES);
//# sourceMappingURL=app.routing.js.map