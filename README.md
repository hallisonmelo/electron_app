## Preparando para uso

Para clonar e executar este repositório você precisará
[Git](https://git-scm.com) e [Node.js](https://nodejs.org/en/download/) 


Depois de instalado:

```bash
# Clone o repositório
git clone https://github.com/electron/electron-quick-start
# Instale as dependências
npm install
npm install -g concurrently
npm install -g lite-server
npm install -g typescript
# Excecute o app
npm start

#me siga
`@hallss93` - Instagram
`Hallison Melo`
```
Learn more about Electron and its API in the [documentation](http://electron.atom.io/docs/).

#### License [CC0 1.0 (Public Domain)](LICENSE.md)
